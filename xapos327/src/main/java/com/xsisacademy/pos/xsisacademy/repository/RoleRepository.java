package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Category;
import com.xsisacademy.pos.xsisacademy.model.Product;
import com.xsisacademy.pos.xsisacademy.model.Role;



public interface RoleRepository extends JpaRepository<Role, Long>{
	
	@Query(value = "SELECT r FROM Role r where isDelete = false")
	List<Role> findByRoles();
	
	@Query(value =  "select * from role where lower(role_name) like lower(concat('%', ?1, '%')) and is_delete = ?2 order by role_name asc", nativeQuery = true)
	Page<Role> findByIsDelete(String keyword , Boolean isDelete, Pageable pagingSort);
	
	@Query(value =  "select * from role where lower(role_name) like lower(concat('%', ?1, '%')) and is_delete = ?2 order by role_name desc", nativeQuery = true)
	Page<Role> findByIsDeleteDesc(String keyword , Boolean isDelete, Pageable pagingSort);

}
