package com.xsisacademy.pos.xsisacademy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.pos.xsisacademy.model.Variant;

public interface VariantRepository extends JpaRepository<Variant, Long>{
	@Query(value = "SELECT v FROM Variant v where active = true")
	List<Variant> findByVariants();
	
	
	@Query(value= "SELECT v FROM Variant v where v.active = true and v.categoryId = ?1 order by name")
	List<Variant> findByCategoryId(Long CategoryId);

}
