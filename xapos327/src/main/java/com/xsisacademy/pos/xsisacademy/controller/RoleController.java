package com.xsisacademy.pos.xsisacademy.controller;

import java.util.Date;
import java.util.List;

import javax.management.relation.Role;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xsisacademy.pos.xsisacademy.model.Category;
import com.xsisacademy.pos.xsisacademy.model.vmCategory;
import com.xsisacademy.pos.xsisacademy.repository.CategoryRepository;
import com.xsisacademy.pos.xsisacademy.repository.RoleRepository;

@Controller
@RequestMapping("/role/")
public class RoleController {
	
	@Autowired
	private RoleRepository roleRepository;
	
	@GetMapping("indexapi")
	public ModelAndView indexapi() {
		ModelAndView view = new ModelAndView("role/indexapi.html");
		return view;
	}
	
	@GetMapping("indexapi_page")
	public ModelAndView indexapi_page() {
		ModelAndView view = new ModelAndView("role/indexapi_page.html");
		return view;
	}
	
	
	

}
