package com.xsisacademy.pos.xsisacademy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class HomeController {
	@GetMapping("index")
	public String index() {
		return "index.html";
	}
	@GetMapping("home")
	public String home() {
		return "home.html";
	}
	@GetMapping("calc")
	public String calc() {
		return "calc.html";
	}

}
