package com.xsisacademy.pos.xsisacademy.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.pos.xsisacademy.model.OrderDetail;
import com.xsisacademy.pos.xsisacademy.repository.OrderDetailRepository;



@RestController @RequestMapping("/api/transaction") public class ApiOrderDetailController {

	@Autowired
	private OrderDetailRepository orderDetailRepository;
	
	@PostMapping("orderdetail/add")
	public ResponseEntity<Object> saveOrderDetail(@RequestBody OrderDetail orderDetail){
		orderDetail.active = true;
		orderDetail.createBy = "admin1";
		orderDetail.createDate = new Date();
		
		OrderDetail orderDetailData = this.orderDetailRepository.save(orderDetail);
		
		if(orderDetailData.equals(orderDetail)) {
			return new ResponseEntity<>("Save Item Succes", HttpStatus.OK);
		}
		else {
			return new ResponseEntity<>("Save failed", HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("orderdetailbyheaderid/{headerId}")
	public ResponseEntity<List<OrderDetail>> getOrderByHeaderId(@PathVariable("headerId") Long id){
		try {
			List<OrderDetail> orderdetail = this.orderDetailRepository.findOrderByheaderId(id);
			return new ResponseEntity<>(orderdetail, HttpStatus.OK);
		}
		catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

}
